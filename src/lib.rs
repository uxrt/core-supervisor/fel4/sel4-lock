#![no_std]
extern crate lock_api;
use core::num::NonZeroUsize;

use lock_api::{GetThreadId, GuardSend, Mutex, MutexGuard, RawMutex, ReentrantMutex, ReentrantMutexGuard};

// 1. Define our raw lock type
pub struct RawSeL4Mutex();

// 2. Implement RawMutex for this type
unsafe impl RawMutex for RawSeL4Mutex {
    const INIT: RawSeL4Mutex = RawSeL4Mutex();

    // A spinlock guard can be sent to another thread and unlocked there
    type GuardMarker = GuardSend;
    

    fn lock(&self) {
        //TODO: finish this
    }

    fn try_lock(&self) -> bool {
        true
    }

    unsafe fn unlock(&self) {
        //TODO: finish this
    }
}

pub struct SeL4ThreadID {}

unsafe impl GetThreadId for SeL4ThreadID {
    const INIT: SeL4ThreadID = SeL4ThreadID {};
    fn nonzero_thread_id(&self) -> NonZeroUsize {
        NonZeroUsize::new(1).unwrap()
    }
}

/*
unsafe impl RawRwLock for RawSeL4RwLock {
    const INIT: RawSeL4RwLock = RawSeL4RwLock();

    // A spinlock guard can be sent to another thread and unlocked there
    type GuardMarker = GuardSend;
    

    fn lock_shared(&self) {
        //TODO: finish this
    }

    fn try_lock_shared(&self) -> bool {
        true
    }

    unsafe fn unlock_exclusive(&self) {
        //TODO: finish this
    }
    fn lock_exclusive(&self) {
        //TODO: finish this
    }

    fn try_lock_exclusive(&self) -> bool {
        true
    }

    unsafe fn unlock_exclusive(&self) {
        //TODO: finish this
    }

}
*/
// 3. Export the wrappers. This are the types that your users will actually use.
pub type SeL4Mutex<T> = Mutex<RawSeL4Mutex, T>;
pub type SeL4MutexGuard<'a, T> = MutexGuard<'a, RawSeL4Mutex, T>;

pub type SeL4ReentrantMutex<T> = ReentrantMutex<RawSeL4Mutex, SeL4ThreadID, T>;
pub type SeL4ReentrantMutexGuard<'a, T> = ReentrantMutexGuard<'a, RawSeL4Mutex, SeL4ThreadID, T>;


/*pub type SeL4RwLock<T> = Mutex<RawSeL4RwLock, T>;
pub type SeL4RwLockReadGuard<'a, T> = RwLockReadGuard<'a, RawSeL4RwLock, T>;
pub type SeL4RwLockWriteGuard<'a, T> = RwLockWriteGuard<'a, RawSeL4RwLock, T>;
*/
